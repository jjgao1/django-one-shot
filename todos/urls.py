from django.urls import path
from todos.views import show_todo, show_todoitem

urlpatterns = [
    path("<int:id>/", show_todoitem, name="todo_list_detail"),
    path("", show_todo, name="todo_list_list")
]

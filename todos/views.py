from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def show_todo(request):
    todo = TodoList.objects.all()
    context = {
        "todo": todo
    }
    return render(request, "todos/list.html", context)

def show_todoitem(request, id):
    item = TodoList.objects.get(id=id)
    context = {
        "item": item,
    }
    return render(request, "todos/detail.html", context)
